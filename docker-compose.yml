version: "3.4"

x-zammad-vars: &common-zammad-variables
  MEMCACHE_SERVERS: "zammad-memcached:11211"
  REDIS_URL: "redis://zammad-redis:6379"

x-metamigo-vars: &common-metamigo-variables
  DATABASE_HOST: "metamigo-postgresql"
  DATABASE_NAME: "metamigo"
  DATABASE_ROOT_OWNER: "root"
  DATABASE_ROOT_PASSWORD: ${METAMIGO_DATABASE_ROOT_PASSWORD}
  DATABASE_OWNER: "metamigo"
  DATABASE_PASSWORD: ${METAMIGO_DATABASE_PASSWORD}
  DATABASE_VISITOR: "app_visitor"
  DATABASE_AUTHENTICATOR: "app_graphile_auth"
  DATABASE_AUTHENTICATOR_PASSWORD: ${METAMIGO_DATABASE_AUTHENTICATOR_PASSWORD}
  DATABASE_URL: "postgresql://metamigo:${METAMIGO_DATABASE_PASSWORD}@metamigo-postgresql/metamigo"
  WORKER_DATABASE_URL: "postgresql://metamigo:${METAMIGO_DATABASE_PASSWORD}@metamigo-postgresql/metamigo"
  SHADOW_DATABASE_URL: "postgresql://metamigo:${METAMIGO_DATABASE_PASSWORD}@metamigo-postgresql/metamigo_shadow"
  ROOT_DATABASE_URL: "postgresql://metamigo:${METAMIGO_DATABASE_PASSWORD}@metamigo-postgresql/template1"
  APP_ROOT_DATABASE_URL: "postgresql://root:${METAMIGO_DATABASE_ROOT_PASSWORD}@metamigo-postgresql/metamigo"
  DATABASE_AUTH_URL: "postgresql://app_graphile_auth:${METAMIGO_DATABASE_AUTHENTICATOR_PASSWORD}@metamigo-postgresql/metamigo"
  CORS_ALLOWED_ORIGINS: "https://metamigo-api,${METAMIGO_DOMAIN},http://localhost:3000,http://127.0.0.1:3000"
  FRONTEND_URL: ${METAMIGO_DOMAIN}
  API_URL: "http://metamigo-api:3001"
  NEXTAUTH_URL: ${METAMIGO_DOMAIN}
  NEXTAUTH_SECRET: ${NEXTAUTH_SECRET}
  NEXTAUTH_AUDIENCE: ${NEXTAUTH_AUDIENCE}
  NEXTAUTH_SIGNING_KEY_B64: ${NEXTAUTH_SIGNING_KEY_B64}
  NEXTAUTH_ENCRYPTION_KEY_B64: ${NEXTAUTH_ENCRYPTION_KEY_B64}
  GITLAB_EMAIL_ADDRESS: ${GITLAB_EMAIL_ADDRESS}
  GITLAB_ID: ${GITLAB_ID}
  GITLAB_SECRET: ${GITLAB_SECRET}
  SIGNALD_ENABLED: "true"
  SIGNALD_SOCKET: /signald/signald.sock

services:
  zammad-elasticsearch:
    environment:
      - discovery.type=single-node
    image: ${ZAMMAD_ELASTICSEARCH_IMAGE}
    restart: ${RESTART}
    volumes:
      - elasticsearch-data:/usr/share/elasticsearch/data

  zammad-init:
    command: ["zammad-init"]
    depends_on:
      - zammad-postgresql
    environment:
      <<: *common-zammad-variables
      POSTGRESQL_USER: zammad
      POSTGRESQL_PASS: ${ZAMMAD_DATABASE_PASSWORD}
    image: ${ZAMMAD_IMAGE}
    restart: on-failure
    volumes:
      - zammad-data:/opt/zammad

  zammad-memcached:
    command: memcached -m 256M
    image: memcached:1.6.10-alpine
    restart: ${RESTART}

  zammad-nginx:
    command: ["zammad-nginx"]
    expose:
      - "80"
    ports:
      - 127.0.0.1:8001:80
    depends_on:
      - zammad-railsserver
    image: ${ZAMMAD_IMAGE}
    restart: ${RESTART}
    environment:
      VIRTUAL_HOST: ${ZAMMAD_VIRTUAL_HOST}
      VIRTUAL_PORT: 80

    volumes:
      - zammad-data:/opt/zammad

  zammad-postgresql:
    environment:
      - POSTGRES_USER=zammad
      - POSTGRES_PASSWORD=${ZAMMAD_DATABASE_PASSWORD}
    image: ${ZAMMAD_POSTGRES_IMAGE}
    restart: ${RESTART}
    volumes:
      - postgresql-data:/var/lib/postgresql/data

  zammad-railsserver:
    command: ["zammad-railsserver"]
    depends_on:
      - zammad-memcached
      - zammad-postgresql
      - zammad-redis
    environment: *common-zammad-variables
    image: ${ZAMMAD_IMAGE}
    restart: ${RESTART}
    volumes:
      - zammad-data:/opt/zammad

  zammad-redis:
    image: redis:6.2.5-alpine
    restart: ${RESTART}

  zammad-scheduler:
    command: ["zammad-scheduler"]
    depends_on:
      - zammad-memcached
      - zammad-railsserver
      - zammad-redis
    environment: *common-zammad-variables
    image: ${ZAMMAD_IMAGE}
    restart: ${RESTART}
    volumes:
      - zammad-data:/opt/zammad

  zammad-websocket:
    command: ["zammad-websocket"]
    depends_on:
      - zammad-memcached
      - zammad-railsserver
      - zammad-redis
    environment: *common-zammad-variables
    image: ${ZAMMAD_IMAGE}
    restart: ${RESTART}
    volumes:
      - zammad-data:/opt/zammad

  metamigo-api:
    image: ${METAMIGO_IMAGE}
    container_name: metamigo-api
    restart: ${RESTART}
    command: ["api"]
    expose:
      - "3001"
    environment: *common-metamigo-variables
    volumes:
      - ./signald:/signald

  metamigo-frontend:
    image: ${METAMIGO_IMAGE}
    container_name: metamigo-frontend
    restart: ${RESTART}
    command: ["frontend"]
    expose:
      - "3000"
    ports:
      - 127.0.0.1:8002:3000
    environment:
      <<: *common-metamigo-variables
      VIRTUAL_HOST: ${METAMIGO_VIRTUAL_HOST}
      VIRTUAL_PORT: 3000

  metamigo-worker:
    image: ${METAMIGO_IMAGE}
    container_name: metamigo-worker
    restart: ${RESTART}
    command: ["worker"]
    environment: *common-metamigo-variables

  metamigo-postgresql:
    image: ${METAMIGO_POSTGRES_IMAGE}
    container_name: metamigo-postgresql
    restart: ${RESTART}
    volumes:
      - metamigo-data:/var/lib/postgresql/data
      - ./scripts/bootstrap-metamigo.sh:/docker-entrypoint-initdb.d/bootstrap-metamigo.sh
    environment:
      <<: *common-metamigo-variables
      POSTGRES_PASSWORD: ${METAMIGO_DATABASE_ROOT_PASSWORD}
      POSTGRES_USER: "root"
      POSTGRES_DB: "metamigo"
    expose:
      - "5432"
    ports:
      - 127.0.0.1:5432:5432

  signald:
    image: ${SIGNALD_IMAGE}
    restart: ${RESTART}
    user: ${CURRENT_UID}
    volumes:
      - ./signald:/signald

  nginx-proxy:
    image: ${NGINX_IMAGE}
    restart: ${RESTART}
    ports:
      - "80:80"
    volumes:
      - /var/run/docker.sock:/tmp/docker.sock:ro

volumes:
  elasticsearch-data:
    driver: local
  postgresql-data:
    driver: local
  zammad-data:
    driver: local
  metamigo-data:
    driver: local
