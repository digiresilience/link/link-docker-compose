CURRENT_UID := $(shell id -u):$(shell id -g)

# don't use this to generate passwords for production
generate-secrets:
	ZAMMAD_DATABASE_PASSWORD=$(shell openssl rand -hex 16)
	METAMIGO_DATABASE_ROOT_PASSWORD=$(shell openssl rand -hex 16)
	METAMIGO_DATABASE_PASSWORD=$(shell openssl rand -hex 16)
	METAMIGO_DATABASE_AUTHENTICATOR_PASSWORD=$(shell openssl rand -hex 16)
	NEXTAUTH_AUDIENCE=$(shell openssl rand -hex 16)
	NEXTAUTH_SECRET=$(shell openssl rand -hex 16)

generate-keys:
	docker exec -i $(shell docker ps -aqf "name=metamigo-frontend") bash -c "/opt/metamigo/cli gen-jwks"

setup-signal:
	mkdir -p signald

create-admin-user:
	docker exec -i $(shell docker ps -aqf "name=metamigo-postgresql") bash < ./scripts/create-admin-user.sh

force-run-migrations: 
	docker-compose -p link-docker-compose exec zammad-railsserver bundle exec rails r 'require "/opt/zammad/db/addon/cdr_signal/20210525091356_cdr_signal_channel.rb";require "/opt/zammad/db/addon/cdr_voice/20210525091357_cdr_voice_channel.rb";require "/opt/zammad/db/addon/cdr_whatsapp/20210525091358_cdr_whatsapp_channel.rb"; require "/opt/zammad/db/addon/pgpsupport/20220403000001_pgpsupport.rb";CdrSignalChannel.new.up;CdrVoiceChannel.new.up;CdrWhatsappChannel.new.up;PGPSupport.new.up;'

start:
	CURRENT_UID=$(CURRENT_UID) docker-compose up -d

restart:
	CURRENT_UID=$(CURRENT_UID) docker restart $(shell docker ps -a -q)

stop:
	CURRENT_UID=$(CURRENT_UID) docker-compose down

destroy:
	docker-compose down
	docker volume prune
