echo "Creating Metamigo admin user"
psql -Xv ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$DATABASE_NAME" <<EOF
INSERT INTO app_public.users(email, name, user_role, is_active, created_by)
VALUES('$GITLAB_EMAIL_ADDRESS', 'Admin', 'admin'::app_public.role_type, true, '')
on conflict (email) do update set user_role = 'admin'::app_public.role_type, is_active = true;
EOF
