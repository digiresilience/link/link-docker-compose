# Welcome

This repository provides a reference Docker Compose file to get you up and running with CDR Link for evaluation and testing purposes.

Link is comprised of two main groups of containers: 1) Zammad, the open source ticket helpdesk that Link is based on and 2) Metamigo, the app that bridges Signal, WhatsApp and Twilio Voice calls to Zammad.

To get started:

1. Install `make` (or if you'd rather not, open the Makefile and cut/paste the commands)

2. Run `make generate-secrets` and replace the equivalent empty vars in the .env file with the output of that command

3. Create a Gitlab application which we'll use for Metamigo authentication: https://gitlab.com/-/profile/applications

   - Scopes should be set to 'read_user'
   - Callback URLs should be be equivalent to whatever value you've set for the METAMIGO_DOMAIN var in the .env file. For example:

     - http://localhost:8002
     - http://localhost:8002/api/auth/callback/gitlab

     or

     - http://metamigo.example.com
     - http://metamigo.example.com/api/auth/callback/gitlab

4. Enter the email address you use to sign into Gitlab, the application ID and application secret in the .env file

5. Run `make setup-signal` to create a required directory

6. Run `make start` to bring up the containers

7. Once the containers have started, we need to generate another set of secrets: run `make generate-keys` and copy the value of "signingKeyB64" to the "NEXTAUTH_SIGNING_KEY_B64" variable in the .env file. Copy the value of "encryptionKeyB64" to the NEXTAUTH_ENCRYPTION_KEY_B64 variable

8. Then stop and start the containers: `make stop` then `make start`

9. Finally, we need to create the Metamigo admin user (this will use the Gitlab email address we entered previously): `make create-admin-user`

**TEMPORARY**

10. Due to an issue with our Zammad Dockerfile, you will need to force the database migrations for our addons to run: `make force-run-migrations`

After you have configured and started the application, you will be able to access Zammad at http://localhost:8001 and Metamigo at http://localhost:8002.

## Reverse proxy

To run Link in production, you will likely need a reverse-proxy to route traffic to the two main public-facing containers ("zammad-nginx" and "metamigo-frontend").

This repo now includes an example Nginx reverse-proxy container. In order to make use of this feature, set the variables ZAMMAD_VIRTUAL_HOST and METAMIGO_VIRTUAL_HOST in the .env file. Then point a DNS record or load-balancer to port 80 on your server. Requests will be correctly routed based on the domain.

The details of a production reverse-proxy configuration will vary between deployments. In particular you will need to determine where to terminate SSL,since this example configuration does not make any assumptions about where certificates will come from. In production we often use LetsEncrypt via the [nginxproxy/acme-companion](https://hub.docker.com/r/nginxproxy/acme-companion) container.

Center for Digital Resilience has a lot of experience hosting the Link stack and can provide some guidance based on your requirements. Contact us at [info@digiresilience.org](mailto:info@digiresilience.org).

## Ansible

We use Ansible to deploy Link. While we cannot provide a full, ready-to-use deployment playbook at this time, what follows is a list of the roles we typically apply to get to a hardened Debian instance running Docker Compose suitable for running Link.

- https://gitlab.com/guardianproject-ops/ansible-debian-ami.git (designed for EC2, but useful as a model elsewhere)
- https://gitlab.com/guardianproject-ops/ansible-unattended_upgrades.git
- https://gitlab.com/guardianproject-ops/ansible-nginx.git
- https://gitlab.com/guardianproject-ops/ansible-docker-host.git
- https://gitlab.com/guardianproject-ops/ansible-firewall-docker.git
- https://gitlab.com/guardianproject-ops/ansible-docker-nginx-proxy.git

You can also adapt your deployment playbooks from ours, which can be found here: https://gitlab.com/guardianproject-ops/ansible-playbooks-link.
